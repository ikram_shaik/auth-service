package com.auth.Controller;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Optional;

import com.auth.Bodies.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.auth.Service.AuthService;


@RestController
public class AuthController {
	private String userMobileNumber;
	private String userDeviceID;
	private String userDeviceModel;
	private long user_Id;

	@Autowired 
	AuthService authService; // Service layer interface
	@Autowired
	loginResponse login_Response; // Body
	@Autowired
	OtpController otp;
	@Autowired
	ComRequestBody requestBody;
	@Autowired
	RequestEntityCheckUser identityRequest;
	@Autowired
	ExceptionResponse exceptionResponse;
	RestTemplate restTemplate=new RestTemplate();

	// Login Endpoint
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody UserLoginDetailInput userInput) {
		//Check for valid input (Entered by user)
		if(!this.authService.ChecknumberValidity(userInput.getUser_name(),userInput.getDevice_model(),userInput.getDevice_id())){
			return ResponseEntity.of(Optional.of("Invalid Username. Please enter valid phone number !!"));
		}
		//Assigning user data to variable so that it can be used in whole class
		userMobileNumber=userInput.getUser_name();
		userDeviceModel = userInput.getDevice_model();
		userDeviceID = userInput.getDevice_id();
		boolean otpSent = false;

		//Check if user have logged in back or not.
		//If yes then will check for token whether it's valid or not.
		//If it's valid then let user enter directly (return token to user)
		if(this.authService.checkUserExists(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID)==1) {
			if(this.authService.verifyTokenExpiry(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID)==1) {
				// return here AuthToken
				login_Response.setAuthToken(this.authService.getValidAuthToken(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID));
				login_Response.setDeviceId(userDeviceID);
				return ResponseEntity.of(Optional.of(login_Response));
			}

			// if token has been expired then generate OTP to create new token.
			user_Id=this.authService.getByUserId(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID);
			int generatedOtp=this.authService.generateOTP();
			System.out.println("User exists and  OTP generated");
			this.authService.storeGeneratedOTP(generatedOtp);
			requestBody.setRequestType("LOTP");
			HashMap<String,String> temp=new HashMap<String,String>();
			temp.put("mobileNumber",userMobileNumber);
			temp.put("otp",String.valueOf(generatedOtp));
			temp.put("userId",Long.toString(user_Id));
			requestBody.setDetails(temp);
			try{
				final String otpurl="http://172.31.15.239:8080/sendSMS"; //Communication API to send OTP to user
				RestTemplate restTemplate=new RestTemplate();
				ComResponseBody isResponse=restTemplate.postForObject(otpurl,requestBody,ComResponseBody.class);
				otpSent = isResponse.isStatus();
			}
			catch(Exception e){
				//e.printStackTrace();
				exceptionResponse.setHttpExceptions("INTERNAL_SERVER_ERROR");
				exceptionResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				return ResponseEntity.of(Optional.of(exceptionResponse));
			}
			//Check communication response if OTP send then will proceed further.
			if(otpSent) {
				Timestamp otpexpiry = this.authService.getOtpExpiryTime();
				System.out.println("OTP Sent to user");
				while(this.authService.checkOtpExpiry(otpexpiry)==1) {
					if(this.otp.getUserOtp()!=0) {
						if(this.authService.verifyOtp(generatedOtp, this.otp.getUserOtp())) {
							this.otp.setUserOtp(0);
							login_Response.setAuthToken(this.authService.saveUser(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID,user_Id,this.authService.generateAuthToken(),userDeviceID));
							login_Response.setDeviceId(userDeviceID);
							return ResponseEntity.of(Optional.of(login_Response));
						}
					}
				}
				return ResponseEntity.of(Optional.of("OTP Expired. So, please login again :)"));
			}
			exceptionResponse.setHttpExceptions("NON_AUTHORITATIVE_INFORMATION");
			exceptionResponse.setStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
			return ResponseEntity.of(Optional.of(exceptionResponse));
		}


		//Call identity service to check its New user or New Device
		identityRequest.setMobile(userMobileNumber);
		identityRequest.setDeviceModel(userDeviceModel);
		identityRequest.setDeviceId(String.valueOf(userDeviceID));
		try {
			String isUrl="http://172.31.33.44:8080/api/user/checkUserExists"; //IS API endpoint
			ApiResponseCheckUser identityResponse= restTemplate.postForObject(isUrl, identityRequest, ApiResponseCheckUser.class);
			user_Id=identityResponse.getUserId();
		}catch(Exception e) {
			//e.printStackTrace();
			exceptionResponse.setHttpExceptions("INTERNAL_SERVER_ERROR");
			exceptionResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return ResponseEntity.of(Optional.of(exceptionResponse));
		}
		if(user_Id<0) {
			return ResponseEntity.of(Optional.of("Account not exists. Please do Signup"));
		}


		//Generate OTP in case of new device or new user
		int generatedOtp = this.authService.generateOTP();
		this.authService.storeGeneratedOTP(generatedOtp);
		System.out.println("After signup OTP generated");
		requestBody.setRequestType("LOTP");
		HashMap<String,String> temp=new HashMap<String,String>();
		temp.put("mobileNumber",userMobileNumber);
		temp.put("otp",String.valueOf(generatedOtp));
		temp.put("userId",Long.toString(user_Id));
		requestBody.setDetails(temp);		//call comm API

		try{
			final String otpurl="http://172.31.15.239:8080/sendSMS";
			RestTemplate restTemplate=new RestTemplate();
			ComResponseBody isResponse=restTemplate.postForObject(otpurl,requestBody,ComResponseBody.class);
			otpSent=isResponse.isStatus();
		}catch (Exception e){
			//e.printStackTrace();
			exceptionResponse.setHttpExceptions("INTERNAL_SERVER_ERROR");
			exceptionResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return ResponseEntity.of(Optional.of(exceptionResponse));
		}

		if(otpSent) {
			System.out.println("otp sent by communication Api");
		    Timestamp otpexpiry = this.authService.getOtpExpiryTime();

			System.out.println("OTP Sent to user. Expires at "+otpexpiry);
			while(this.authService.checkOtpExpiry(otpexpiry)==1) {
				if(this.otp.getUserOtp()!=0) {
					if(this.authService.verifyOtp(generatedOtp, this.otp.getUserOtp())) {
						this.otp.setUserOtp(0);
						login_Response.setAuthToken(this.authService.saveUser(userMobileNumber+"-"+userDeviceModel+"-"+userDeviceID,user_Id,this.authService.generateAuthToken(),userDeviceID));
						login_Response.setDeviceId(userDeviceID);
						return ResponseEntity.of(Optional.of(login_Response));
					}
				}
			}
			return ResponseEntity.of(Optional.of("OTP Expired. So, please login again :)"));
		}
		exceptionResponse.setHttpExceptions("NON_AUTHORITATIVE_INFORMATION");
		exceptionResponse.setStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION.value());
		return ResponseEntity.of(Optional.of(exceptionResponse));

	}


	//API will be called by LOS and PS to validate authtoken
	@RequestMapping(value="/validateToken", method=RequestMethod.POST)
	public HashMap<String,Integer> validateToken(@RequestBody HashMap<String,String> validateTokenRequest) {
		System.out.println("Someone asking to validate the AuthToken !!");
		HashMap<String,Integer> h = new HashMap<>();
		try{
			//will call our validateToken() method to check token is valid or not
			if(authService.validateToken(validateTokenRequest.get("auth_token"),validateTokenRequest.get("device_id"))==1) // if token is
			{
				h.put("Status",HttpStatus.OK.value());
				System.out.println("This Auth Token is valid");
				return h;
			}
		}catch (Exception e){
			//e.printStackTrace();
			h.put("Status",HttpStatus.INTERNAL_SERVER_ERROR.value());
			return h;
		}
		System.out.println("This Auth Token is expired");
		h.put("Status",HttpStatus.UNAUTHORIZED.value());
		return h;
	}
}
