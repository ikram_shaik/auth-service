package com.auth.Bodies;

//class to accept user-entered otp
public class OtpRequest {

    private int otp;

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public OtpRequest() {
        super();
    }

    public OtpRequest(int otp) {
        this.otp = otp;
    }
}
