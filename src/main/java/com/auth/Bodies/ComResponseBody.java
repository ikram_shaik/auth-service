package com.auth.Bodies;

//response body from communication service with the status(acknowledgement that OTP Sent or not)
public class ComResponseBody {
	private boolean status;

	public ComResponseBody() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ComResponseBody(boolean status) {
		super();
		this.status = status;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
